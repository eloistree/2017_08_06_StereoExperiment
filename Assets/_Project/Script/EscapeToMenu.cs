﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeToMenu : MonoBehaviour {

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            GoToMenu();
    }


	// Use this for initialization
	public void GoToMenu () {
        GoToScene(0);
    }

    // Update is called once per frame
    public void GoToScene(int sceneNumber)
    {

        SceneManager.LoadScene(sceneNumber);
    }
    public void GoToScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);

    }
}
