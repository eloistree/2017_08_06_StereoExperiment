﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderToCameraEyes : MonoBehaviour {

    public CameraEyeesDistance _cameraEyesPosition;
    public Slider _uiSlider;
    public Text _uiText;
    // Use this for initialization
    void Start () {
        _uiSlider.onValueChanged.AddListener(ChangeCameraPosition);
	}

    private void ChangeCameraPosition(float value)
    {
        _uiText.text =""+ value;
        _cameraEyesPosition.SetEyesDistance(value);
    }
    
}
