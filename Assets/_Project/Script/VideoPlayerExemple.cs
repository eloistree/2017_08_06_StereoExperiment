﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoPlayerExemple : MonoBehaviour
{
    public GameObject _camera = null;
    public VideoPlayer _videoPlayerLow = null;
    public VideoPlayer _videoPlayerHight = null;
    public string _videoUrlLow;
    public string _videoUrlHight;
    public string _videoAndroidPathLow;
    public string _videoAndroidPathUrlHight;
    [Range(0f,1f)]
    public float _pourcent=0.5f;

    [Header("Debug")]
    public long _wantedFrame;
    public long _maxFrame;
    public bool _isInitialized;
    public float _waitBeforeDisplay=4f;
    IEnumerator Start()
    {
        if (_camera == null)
            _camera = GameObject.Find("Main Camera");
#if UNITY_EDITOR


#elif UNITY_ANDROID
                     WWW wwwLow = new WWW(_videoUrlLow);
        yield return wwwLow;
        _videoUrlLow = _videoAndroidPathLow;
        File.WriteAllBytes(_videoAndroidPathLow, wwwLow.bytes);


        WWW wwwHight = new WWW(_videoUrlHight);
        yield return wwwHight;
        _videoUrlHight = _videoAndroidPathUrlHight;
        File.WriteAllBytes(_videoAndroidPathUrlHight, wwwHight.bytes);
#endif

        SetVideoPlayer(ref _videoPlayerLow, _videoUrlLow);
        SetVideoPlayer(ref _videoPlayerHight, _videoUrlHight);
        _isInitialized = true;
        yield return new WaitForSeconds(_waitBeforeDisplay);
        SetTextureWithMaxTo(0);
        yield break;
        
    }


    private void SetVideoPlayer(ref VideoPlayer videoPlayer, string url)
    {
        videoPlayer = _camera.AddComponent<UnityEngine.Video.VideoPlayer>();
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;
        videoPlayer.targetCameraAlpha = 0f;
        videoPlayer.url = url;
        videoPlayer.isLooping = true;
        videoPlayer.frame=0;
        videoPlayer.Play();
        videoPlayer.Pause();
    }

   
    
    public long GetFrameByPourcent(float pourcent)
    {
        return (long)(pourcent * (float)_videoPlayerLow.frameCount);
    }
    public long GetFrameByPourcentHight(float pourcent)
    {
        return (long)(pourcent * (float)_videoPlayerHight.frameCount);
    }
    

    public RawImage _image;
    public void SetTextureTo(float pourcent)
    {
        SetTextureTo(GetFrameByPourcent(pourcent));
    }
    public void SetTextureTo(long frame)
    {
        _videoPlayerLow.frame = frame;
        _image.texture = _videoPlayerLow.texture;

    }
    public void SetTextureWithMaxTo(float pourcent)
    {
        SetTextureWithMaxTo(GetFrameByPourcentHight(pourcent));
    }
    public void SetTextureWithMaxTo(long frame)
    {
        StartCoroutine(SetTextureWithMaxToCoroutine(frame));
    }
    public IEnumerator SetTextureWithMaxToCoroutine(long frame)
    {
        //_videoPlayerLow.frame = frame;
        //_image.texture = _videoPlayerLow.texture;
        _videoPlayerHight.frame = frame;
        Texture image = _videoPlayerHight.texture;
        yield return new  WaitForSeconds(0.8f );
        _image.texture = image;
    }
    //void EndReached(UnityEngine.Video.VideoPlayer vp)
    //{
    //    vp.playbackSpeed = vp.playbackSpeed / 10.0F;
    //}
}