﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickScript_ImageSwitcher : MonoBehaviour {

    public Image _affectedUI;
    public Sprite[] _images;

    public float _switchingTimer = 5f;

    [Header("Debug")]
    public int _imageIndex;
	// Use this for initialization
	IEnumerator Start () {

        while (true)    
        {
            SwitchImage();
            yield return new WaitForSeconds(_switchingTimer);
            if (_switchingTimer <= 0f) yield break;
        }
	}

    private void SwitchImage()
    {
        if (_images.Length == 0)
            return;

        if (_imageIndex >= _images.Length)
            _imageIndex = 0;

        _affectedUI.sprite = _images[_imageIndex];
        _imageIndex++;

    }
    
}
