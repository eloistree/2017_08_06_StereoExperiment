﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.Events;

public class ScreenshotManager : MonoBehaviour {

    public string _pathFolder;
    public string _pathFolderAndroid;

    [Range(0f,10f)]
    public int _resolutionMulticator=1;

    public UnityEvent _beforeScreenshot;
    public UnityEvent _afterScreenshot;


    public void TakeScreenShot()
    {
       StartCoroutine(TakeScreenShotCoroutine(DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_")));
    }
    public void TakeScreenShot(int indexName)
    {

        StartCoroutine(TakeScreenShotCoroutine("" + indexName));
    }
    public void TakeScreenShot(string idName)
    {

        StartCoroutine(TakeScreenShotCoroutine("" + idName));
    }
    public enum ScreenshotType { PNG, JPG, JPEG}
    public IEnumerator TakeScreenShotCoroutine(string idValue, string screenshotName ="Screenhot", ScreenshotType  type = ScreenshotType.JPG)
    {
        _beforeScreenshot.Invoke();
        yield return new WaitForEndOfFrame();
#if UNITY_EDITOR
#elif UNITY_ANDROID
        _pathFolder = _pathFolderAndroid;
#endif

        if (!Directory.Exists(_pathFolder))
            Directory.CreateDirectory(_pathFolder);

        string filePath = _pathFolder + string.Format("/{0}_{1}.{2}", screenshotName, idValue, type.ToString());
        if (File.Exists(filePath))
            File.Delete(filePath);
        Application.CaptureScreenshot(filePath,_resolutionMulticator);

        _afterScreenshot.Invoke();

    }

    public void OnValidate()
    {
        if (string.IsNullOrEmpty(_pathFolder)) {
            _pathFolder = Application.dataPath+@"/../Screencapture";
        }
    }
}
