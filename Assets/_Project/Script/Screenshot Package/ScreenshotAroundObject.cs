﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotAroundObject : MonoBehaviour {

    public ScreenshotManager _screenshotManager;
    public Transform _objectToRotate;
    public int _subdivision = 360;
    public bool _pauseGame = true;
    public bool _openGifMakerWebsiteAfter;

    public void TakeScreenshotAround(Transform direction) {
       StartCoroutine( TakeScreenshotAround(direction.position, direction.rotation) );
    }
    IEnumerator TakeScreenshotAround(Vector3 initialPosition, Quaternion initialRotation) {

        if (_pauseGame)
            Time.timeScale = 0;
        float angle = 0;
        int subdivision = _subdivision;
        for (int i = 0; i < subdivision; i++)
        {
            angle = (360f / (float) subdivision) * (float) i;
            print("Angle: " + angle);
            _objectToRotate.position = initialPosition;
            _objectToRotate.rotation = initialRotation  *  Quaternion.Euler(0, angle, 0);

            yield return new WaitForEndOfFrame();
            _screenshotManager.TakeScreenShot("AroundShot_"+i);
        }

        if (_pauseGame)
            Time.timeScale = 1;

        if (_openGifMakerWebsiteAfter)
            Application.OpenURL("http://gifmaker.me/");

    

    }
}
