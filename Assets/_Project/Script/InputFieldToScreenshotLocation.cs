﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldToScreenshotLocation : MonoBehaviour {

    public ScreenshotManager _screenshotManager;
    public InputField _screenshotPath;

	void Start () {


        _screenshotPath.onValueChanged.AddListener(ChangeFolderPath);
        if (string.IsNullOrEmpty(_screenshotPath.text))
            DefineDataRootAsDefaultPlace();
    }

    private void DefineDataRootAsDefaultPlace()
    {
        _screenshotPath.text = Application.dataPath ;
    }

    private void ChangeFolderPath(string newPath)
    {
        _screenshotManager._pathFolder = newPath;
        if (string.IsNullOrEmpty(_screenshotPath.text))
            DefineDataRootAsDefaultPlace();

    }
}
