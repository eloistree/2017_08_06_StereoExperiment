﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePause : MonoBehaviour {

    
    public void SetPause(bool setAsOn) {
        Time.timeScale = setAsOn ? 0f:1f;
    }
    public void SwitchGameState() {
            SetPause(!(Time.timeScale <= 0f));
    }
}
