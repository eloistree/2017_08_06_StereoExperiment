﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEyeesDistance : MonoBehaviour {

    [Range(0f,42f)]
    public float _eyeesDistance = 0.06f;
    public Transform _leftCameraAnchor;
    public Transform _rightCameraAnchor;

    public void SetEyesDistance(float distance) {
        _eyeesDistance = distance;
        RefreshCameraPosition();
    }

    public void OnValidate()
    {
        RefreshCameraPosition();
    }
    public void    RefreshCameraPosition()
    {
        _leftCameraAnchor.localPosition = new Vector3(-_eyeesDistance / 2f, 0, 0);
        _rightCameraAnchor.localPosition = new Vector3(_eyeesDistance / 2f, 0, 0);
        _leftCameraAnchor.localRotation = Quaternion.identity;
        _rightCameraAnchor.localRotation = Quaternion.identity;
    }
}
