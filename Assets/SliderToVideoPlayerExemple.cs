﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderToVideoPlayerExemple : MonoBehaviour  {

    public VideoPlayerExemple _videoPlayer;
    public Slider _slider;
    public bool _isPressing;
    public bool _hasBeenDragUp;
    public float _lastValue;
    void Start ()
    {
        _slider.onValueChanged.AddListener(ChangeVideoFrame);
        _videoPlayer.SetTextureTo(0);
        _videoPlayer.SetTextureWithMaxTo(0);
    }
    public void Update()
    {
        if (!_hasBeenDragUp) return;
        
        bool isPressing= Input.GetMouseButton(0) || Input.touchCount>0;
        if (isPressing != _isPressing) {

            if (isPressing == false && _hasBeenDragUp) {
                _hasBeenDragUp = false;

                _videoPlayer.SetTextureWithMaxTo(_lastValue);
            }
            _isPressing = isPressing;
        }
    }

    private void ChangeVideoFrame(float value)
    {
        _hasBeenDragUp = true;
        _lastValue = value;
        _videoPlayer.SetTextureTo(_lastValue);
    }
    
}
