﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderToVideoPlayerSpeedExemple : MonoBehaviour {

    public VideoPlayerSpeedExemple _videoPlayer;
    public Slider _slider;
    public bool _isPressing;
    public bool _hasBeenDragUp;
    public float _lastValue;
    void Start()
    {
        _slider.onValueChanged.AddListener(ChangeVideoFrame);
    }
   
    private void ChangeVideoFrame(float value)
    {

        _videoPlayer.SetWantedPourcent(value);
    }
}
