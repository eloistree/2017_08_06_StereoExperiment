﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoPlayerSpeedExemple : MonoBehaviour
{
    public GameObject _camera = null;
    public VideoPlayer _videoPlayerHight = null;
    public string _videoUrl;
    public string _videoAndroidPath;

    [Range(0f, 10f)]
    public float _videoSpeed = 3f;
    [Range(0f, 1f)]
    public float _arrivedTolerence = 0.03f;

    [Range(0f, 1f)]
    public float _pourcent = 0f;

    [Range(0f, 1f)]
    public float _wantedPourcent = 0.5f;

    internal void SetWantedPourcent(float value)
    {
        _wantedPourcent = value;
    }

    public bool _hasBeenInitialized;

    IEnumerator Start()
    {
#if UNITY_EDITOR

#elif UNITY_ANDROID
            WWW www = new WWW(_videoUrl);
            yield return www;
            _videoUrl= _videoAndroidPath;
            File.WriteAllBytes(_videoAndroidPath, www.bytes);
#endif

        if (_camera == null)
            _camera = GameObject.Find("Main Camera");

       
        
        SetVideoPlayer(ref _videoPlayerHight, _videoUrl);
        _hasBeenInitialized = true;
        yield break;

    }


    private void SetVideoPlayer(ref VideoPlayer videoPlayer, string url)
    {
        videoPlayer = _camera.AddComponent<UnityEngine.Video.VideoPlayer>();
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;
        videoPlayer.targetCameraAlpha = 1f;
        videoPlayer.url = url;
        videoPlayer.isLooping = true;
        videoPlayer.frame = 0;
        _videoPlayerHight.playbackSpeed = 0.1f;
        videoPlayer.Play();
    }



    public long GetFrameByPourcent(float pourcent)
    {
        return (long)(pourcent * (float)_videoPlayerHight.frameCount);
    }


    public void Update()
    {
        if (!_hasBeenInitialized)
            return;

        if (Distance(_wantedPourcent,_pourcent)> _arrivedTolerence)
        {
            _videoPlayerHight.playbackSpeed = _videoSpeed;
            _pourcent = GetPourcentOfCurrentVideo();



        }
        else _videoPlayerHight.playbackSpeed = 0f;


    }

    private float Distance(float _wantedPourcent, float _pourcent)
    {
        return Mathf.Abs( Mathf.Abs(_wantedPourcent) - Mathf.Abs(_pourcent));
    }

    private float GetPourcentOfCurrentVideo()
    {
        return _videoPlayerHight.frame / (float)_videoPlayerHight.frameCount;
    }
}